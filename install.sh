#!/bin/bash

PKGS=(
    # xorg stuffs
    'xorg'
    'xorg-server'
    'xorg-xinit'
    'xterm'

    # other stuffs
    'base-devel'
    'firefox'
    'fish'
    'kitty'
    'linux'
    'linux-firmware'
    'linux-headers'
    'networkmanager'
    'nitrogen'
    'numlockx'
    'picom'
    'pipewire'
    'pipewire-pulse'
    'playerctl'
    'qtile'
    'rofi'

    # fonts
    'ttf-fira-code'
    'ttf-roboto'
)

for PKG in "${PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    sudo pacman -S "$PKG" --noconfirm --needed
done

# building YAY
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
cd

# YAY installs
yay -S \
    visual-studio-code-bin \
    spotify

sudo systemctl enable --now NetworkManager.service
sudo systemctl enable --now bluetooth.service

# Theming stuffs
## Wallpaper
cp Wallpapers ~/
nitrogen --set-scaled ~/Wallpapers/tux-penguin.png

## Icon theme for rofi
git clone https://github.com/vinceliuice/McMojave-circle.git
cd McMojave-Cicle
./install.sh
cd

# Set fish to default shell and make it launch in Starship.rs
sh -c "$(curl -fsSL https://starship.rs/install.sh)"

# Copy over dotfiles
cp -r .config ~/.config
cp .dotfiles/.* ~/